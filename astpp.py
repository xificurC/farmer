"""
A pretty-printing dump function for the ast module.  The code was copied from
the ast.dump function and modified slightly to pretty-print.

Alex Leone (acleone ~AT~ gmail.com), 2010-01-30

From http://alexleone.blogspot.co.uk/2010/01/python-ast-pretty-printer.html

----

And this version is copied from

https://bitbucket.org/takluyver/greentreesnakes/src/16e8d3ce9a18dd54278908557c3e4999c20f088d?at=default

and further modified by Peter Nagy (xificurC ~AT~ gmail.com)
"""

from ast import AST, iter_fields, parse

def dump(node, indent='    '):
    """
    Return a formatted dump of the tree in *node*.  This is mainly useful for
    debugging purposes.  The returned string will show the names and the values
    for fields.
    """
    def _format(node, level=0):
        if isinstance(node, AST):
            fields = [(a, _format(b, level + 1)) for a, b in iter_fields(node)]
            fields.extend([(a, _format(getattr(node, a), level + 1))
                           for a in node._attributes])
            if fields:
                return ''.join([
                    node.__class__.__name__,
                    '(\n',
                    (level+1) * indent,
                    (',\n' + ((level+1) * indent)).join('%s=%s' % field for field in fields),
                    ')'])
            return node.__class__.__name__ + '()'
        if isinstance(node, list):
            if not node:
                return '[]'
            lines = (indent * (level + 1) + _format(x, level + 1) + ',' for x in node)
            return '[\n' + '\n'.join(lines) + '\n' + (indent * (level + 1) + ']')
        return repr(node)

    if not isinstance(node, AST):
        raise TypeError('expected AST, got %r' % node.__class__.__name__)
    return _format(node)

def pdp(code, mode="exec", **kwargs):
    """Parse some code from a string and pretty-print it."""
    node = parse(code, mode=mode)   # An ode to the code
    print(dump(node, **kwargs))
